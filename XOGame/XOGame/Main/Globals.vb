﻿''' <summary>
''' This class is used to share object to all points of the game, always be in memory, and accessible.
''' </summary>
''' <remarks></remarks>
Public Class Globals
    Public Shared Content As ContentManager
    Public Shared Graphics As GraphicsDeviceManager
    Public Shared SpriteBatch As SpriteBatch
    Public Shared GameTime As GameTime
    Public Shared WindowFocused As Boolean
    Public Shared GameSize As Vector2
    Public Shared Mouseloc As Vector2
    Public Shared BackBuffer As RenderTarget2D
    Public Shared texture As Texture2D
    Public Shared block1 As Texture2D
    Public Shared OPlayer As Texture2D
    Public Shared XPlayer As Texture2D
    Public Shared wait As Texture2D
    Public Shared positionX, positionY As Integer
    Public Shared Field(6, 6) As Integer
    Public Shared store(6, 6) As Integer
    Public Shared Turn As Integer = 1
    Public Shared title40 As SpriteFont
    Public Shared score20 As SpriteFont
    Public Shared mousePos As SpriteFont
    Public Shared author14 As SpriteFont
    Public Shared instruction14 As SpriteFont

    Public Shared mousePress As Boolean = False
    Public Shared randnum As New Random


End Class
