
Public Class Game1
    Inherits Microsoft.Xna.Framework.Game

    Dim record As Integer = 0
    Dim win As Integer
    Dim countO As Integer = 0
    Dim countX As Integer = 0
    Dim ry As Integer
    Dim rx As Integer
    Const oval = 1
    Const axe = 2
    Const size = 100

    Public Sub New()
        Globals.Graphics = New GraphicsDeviceManager(Me)
        Content.RootDirectory = "Content"
    End Sub

    Protected Overrides Sub Initialize()
        Me.IsMouseVisible = True

        Window.AllowUserResizing = False

        Globals.GameSize = New Vector2(700, 800)
        Globals.Graphics.PreferredBackBufferWidth = Globals.GameSize.X
        Globals.Graphics.PreferredBackBufferHeight = Globals.GameSize.Y
        Globals.Graphics.ApplyChanges()

        Globals.BackBuffer = New RenderTarget2D(Globals.Graphics.GraphicsDevice, Globals.GameSize.X, Globals.GameSize.Y, False,
                                                    SurfaceFormat.Color, DepthFormat.None, 0, RenderTargetUsage.PreserveContents)
        Globals.store(4, 4) = 0
        MyBase.Initialize()
    End Sub


    Protected Overrides Sub LoadContent()
        ' Create a new SpriteBatch, which can be used to draw textures.
        Globals.SpriteBatch = New SpriteBatch(GraphicsDevice)

        'Background's texture
        Globals.texture = Content.Load(Of Texture2D)("Images\\greenflower")
        Globals.wait = Content.Load(Of Texture2D)("Images\\pen0")

        'Grid's texture
        Globals.block1 = Content.Load(Of Texture2D)("Images\\block1")
        

        'OPlayer and XPlayer's textures
        Globals.OPlayer = Content.Load(Of Texture2D)("Images\\O")
        Globals.XPlayer = Content.Load(Of Texture2D)("Images\\X")

        'Load Sprite font
        Globals.title40 = Content.Load(Of SpriteFont)("Font\\Title")
        Globals.score20 = Content.Load(Of SpriteFont)("Font\\Score")
        Globals.mousePos = Content.Load(Of SpriteFont)("Font\\Score")
        Globals.author14 = Content.Load(Of SpriteFont)("Font\\author")
        Globals.instruction14 = Content.Load(Of SpriteFont)("Font\\play")
    End Sub

    

    Protected Overrides Sub Update(ByVal gameTime As GameTime)
        ' TODO: Add your update logic here
        MyBase.Update(gameTime)

        Globals.WindowFocused = Me.IsActive
        Globals.GameTime = gameTime

        'Get mouse location
        Globals.Mouseloc.X = Mouse.GetState.X
        Globals.Mouseloc.Y = Mouse.GetState.Y

        If Globals.Mouseloc.X > 100 And Globals.Mouseloc.X < 600 And Globals.Mouseloc.Y > 200 And Globals.Mouseloc.Y < 700 Then
            Globals.positionX = Globals.Mouseloc.X / size
            Globals.positionY = Globals.Mouseloc.Y / size
        End If


        If Globals.Turn = 1 Then
            If Globals.mousePress = False Then
                If Mouse.GetState.LeftButton = ButtonState.Pressed Then
                    Globals.mousePress = True
                    Globals.Field(Globals.positionX, Globals.positionY) = oval
                    Globals.Turn = 2
                    Globals.store(Globals.positionX, Globals.positionY) = 1
                End If
            End If
        End If
        
        If Globals.Turn = 2 Then
            rx = Globals.randnum.Next(minValue:=1, maxValue:=5)
            ry = Globals.randnum.Next(minValue:=2, maxValue:=6)
            If Globals.store(rx, ry) = 1 Then
                rx = Globals.randnum.Next(minValue:=1, maxValue:=5)
                ry = Globals.randnum.Next(minValue:=2, maxValue:=6)
            End If
            Globals.Field(rx, ry) = axe
            Globals.store(rx - 1, ry - 2) = 2
            Globals.Turn = 1

        End If

        If Mouse.GetState.LeftButton = ButtonState.Released Then
            Globals.mousePress = False
        End If

        
        For y = 2 To 6
            For x = 1 To 5
                If Globals.store(x, y) = 1 Then
                    countO += 1
                ElseIf Globals.store(x, y) = 2 Then
                    countX += 1
                End If
                If countO = 5 Then
                    win = 1
                ElseIf countX = 5 Then
                    win = 2
                End If
            Next
            If countO = 5 Then
                win = 1
            ElseIf countX = 5 Then
                win = 2
            End If
        Next

    End Sub

    Protected Overrides Sub Draw(ByVal gameTime As GameTime)

        Globals.Graphics.GraphicsDevice.SetRenderTarget(Globals.BackBuffer)
        GraphicsDevice.Clear(Color.Beige)
        MyBase.Draw(gameTime)

        Globals.Graphics.GraphicsDevice.SetRenderTarget(Nothing)

        'Draw backbuffer to screen
        Globals.SpriteBatch.Begin() 'Begin sprite

        'Background 
        Globals.SpriteBatch.Draw(Globals.texture, New Vector2(0, 0), Color.White)

        For y = 2 To 6
            For x = 1 To 5

                'Draw 5x5 grid
                Globals.SpriteBatch.Draw(Globals.block1, New Rectangle(x * size, y * size, size, size), Color.White)
                ' Globals.SpriteBatch.Draw(Globals.wait, New Rectangle(Globals.positionX * size, Globals.positionY * size, 50, 50), Color.White)

                'Draw the 2 players of the game
                Select Case Globals.Field(x, y)
                    Case oval
                        Globals.SpriteBatch.Draw(Globals.OPlayer, New Rectangle(x * size, y * size, size, size), Color.White)

                    Case axe
                        Globals.SpriteBatch.Draw(Globals.XPlayer, New Rectangle(x * size, y * size, size, size), Color.White)
                End Select
            Next
        Next

        'Game title 
        Globals.SpriteBatch.DrawString(Globals.title40, "X-O Challenge", New Vector2(150, 20), Color.Chocolate)
        Globals.SpriteBatch.DrawString(Globals.author14, "Author: Kim Phan", New Vector2(250, 90), Color.DarkGreen)
        Globals.SpriteBatch.DrawString(Globals.instruction14, "Player must have 5 O in a row in order to win", New Vector2(100, 110), Color.BlueViolet)


        Globals.SpriteBatch.DrawString(Globals.score20, "Score: ", New Vector2(50, 140), Color.DarkSalmon)
        
        If win = 1 Then
            record += 1
            Globals.SpriteBatch.DrawString(Globals.mousePos, "you win", New Vector2(250, 750), Color.DarkSalmon)
            Globals.SpriteBatch.DrawString(Globals.score20, record.ToString, New Vector2(230, 110), Color.Red)

        ElseIf win = 2 Then
            Globals.SpriteBatch.DrawString(Globals.mousePos, "computer win", New Vector2(250, 750), Color.DarkSalmon)

        End If

        Globals.SpriteBatch.End() 'End sprite

    End Sub

   





End Class
